const sizeTranslation = {
  fine: "fine",
  diminutive: "dim",
  tiny: "tiny",
  small: "sm",
  medium: "med",
  large: "lg",
  huge: "huge",
  gargantuan: "grg",
  colossal: "col",
};

const sizes = Object.keys(sizeTranslation);

const transform = (casters, size) => {
  casters.forEach(c => c.update({'data.traits.size': sizeTranslation[size]}));
};

const tokens = canvas.tokens.controlled;

if (tokens.length < 1) {
  ui.notifications.warn("Select a token.");
} else {
  const actors = tokens.map(o => o.actor);

  const buttons = sizes.map((size) => ({
      label: size,
      callback: () => transform(actors, size),
  }));

  new Dialog({
      title: "Change Size",
      content: "Choose a size.",
      buttons: buttons,
  }).render(true);
}