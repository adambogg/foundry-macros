// SETUP NEEDED for this to work, you need to set castingAbilityScore, constantBonuses & combatBonuses to be correct

const casterClasses = [
  "Adept",
  "Alchemist",
  "Antipaladin",
  "Arcanist",
  "Bard",
  "Bloodrager",
  "Cleric",
  "Druid",
  "Hunter",
  "Inquisitor",
  "Investigator",
  "Magus",
  "Medium",
  "Mesmerist",
  "Occultist",
  "Oracle",
  "Paladin",
  "Psychic",
  "Ranger",
  "Red Mantis Assassin",
  "Sahir-Afiyun",
  "Shaman",
  "Skald",
  "Sorcerer",
  "Spiritualist",
  "Summoner",
  "Summoner (Unchained)",
  "Warpriest",
  "Witch",
  "Wizard",
];

const tokens = canvas.tokens.controlled;

const castingAbilityScore = "wis"; // either "int", "wis", or "cha"
const constantBonuses = 2; // a bonus that is always active e.g. focused mind trait
const combatBonuses = 4; // a bonus that is active only in combat e.g. combat casting feat

const getAbilityMod = (ability) => {
  switch (ability.toLowerCase()) {
    case "int": {
      return token.actor.data.data.abilities.int.mod;
    }
    case "wis": {
      return token.actor.data.data.abilities.wis.mod;
    }
    case "cha": {
      return token.actor.data.data.abilities.cha.mod;
    }
    default: {
      return 0;
    }
  }
};

function rollCheck(castingAbilityMod, casterLevel, miscBonus) {
  const roll = new Roll(
    `1d20 + ${casterLevel} + ${castingAbilityMod} + ${constantBonuses} + ${miscBonus}`
  );
  roll.roll();
  roll.toMessage({
    flavor: "Concentration Check",
    speaker: { alias: token.actor.data.name },
  });
}

if (tokens.length < 1) {
  ui.notifications.warn("No token selected.");
} else {
  tokens.forEach((token) => {
    let casterLevel = 0;

    const classes = token.actor.data.items.filter(
      (item) => item.type === "class"
    );

    for (var i = 0; i < classes.length; i += 1) {
      if (casterClasses.includes(classes[i].name)) {
        if (classes[i].name === "Ranger" || classes[i].name === "Paladin") {
          casterLevel +=
            classes[i].data.level - 4 > 0 ? classes[i].data.level - 4 : 0;
        } else {
          casterLevel += classes[i].data.level;
        }
      }
    }

    const castingAbilityMod = getAbilityMod(castingAbilityScore);
    new Dialog({
      title: "Select Option",
      buttons: {
        out: {
          label: "Out of combat",
          callback: () => rollCheck(castingAbilityMod, casterLevel, 0),
        },
        in: {
          label: "In combat",
          callback: () =>
            rollCheck(castingAbilityMod, casterLevel, combatBonuses),
        },
      },
    }).render(true);
  });
}
