// Channel positive as the currently-selected character.

const tokens = canvas.tokens.controlled;
let caster = tokens.map((o) => o.actor)[0];

function channelPositive() {
  const classes = caster.data.items.filter((item) => item.type === "class");
  const isCleric = classes.some((classItem) => classItem.name === "Cleric");

  if (!isCleric) {
    ui.notifications.warn("You're not a cleric!");
    return;
  }

  const clericLevel = classes.find((item) => item.name == "Cleric").data.level;
  const rollString = `${Math.floor((clericLevel + 1) / 2)}d6`;

  const roll = new Roll(rollString);
  roll.roll();
  roll.toMessage({
    flavor: "Channeling positive energy",
    speaker: { alias: caster.data.name },
  });
}

if (!caster || caster === undefined) {
  ui.notifications.warn("You need to be controlling someone to channel!");
} else {
  channelPositive();
}
