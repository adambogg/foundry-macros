const caster = canvas.tokens.controlled
  .map((t) => t.actor)
  .filter((a) => a.hasPerm(game.user, "OWNER"))[0];
const casterName = caster.data.name;

function speak(message) {
  const chatData = {
    user: game.user._id,
    speaker: { alias: casterName },
    content: message,
  };
  ChatMessage.create(chatData);
}

function rollCureLight(num, patients) {
  const rollString = `${num}d8 + ${num}`;
  const msg = `Using a total of ${num * patients.length} charges.`;
  speak(msg);

  patients.forEach((patient) => {
    const pronoun = casterName === patient ? "themselves" : patient;
    const roll = new Roll(rollString);
    roll.roll();
    roll.toMessage({
      flavor: `Healing ${pronoun}`,
      speaker: { alias: caster.data.name },
    });
  });
}

const actors = game.actors.entities.filter(
  (o) => o.isPC && o.hasPerm(game.user, "OBSERVER")
);

if (!actors.length) {
  ui.notifications.warn("No applicable actor(s) found");
} else {
  const characterNames = actors.map((a) => a.name.split(" ")[0]);
  const msg = `Select which characters you want to heal and how many charges per character:<br> ${characterNames.map(
    (name) =>
      `<span><input type="checkbox" id=${name} value=${name} style="transform: translateY(5px);"><label for=${name}>${name}</label></span>`
  ).join("")}`;

  const buttons = {};
  const chargeAmounts = [1, 5, 10, 15];
  chargeAmounts.forEach((amount) => {
    buttons[amount] = {
      label: String(amount),
      callback: (html) => {
        const patients = [];
        actors.forEach((actor) => {
          const input = html.find(`input[id=${actor.name.split(" ")[0]}`);
          if (input.prop("checked")) {
            patients.push(actor.name);
          }
        });
        rollCureLight(amount, patients);
      },
    };
  });

  new Dialog({
    title: "Batch Heal",
    content: `<p>${msg}</p>`,
    buttons: buttons,
  }).render(true);
}
