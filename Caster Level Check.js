const casterClasses = ["Cleric", "Alchemist", "Druid", "Bard"];

const tokens = canvas.tokens.controlled;

if (tokens.length < 1) {
  ui.notifications.warn("No token selected.");
} else {
  tokens.forEach((token) => {
    let casterLevel = 0;
    
    for (const [_, value] of Object.entries(token.actor.data.data.classes)) {
      if (casterClasses.includes(value.name)) {
        casterLevel += value.level;
      }
    }

    const roll = new Roll(`1d20 + ${casterLevel}`);
    roll.roll();
    roll.toMessage({
      flavor: "Caster Level Check",
      speaker: {alias: token.actor.data.name}
    });
  });
}
