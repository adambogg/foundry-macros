const pdf = ``;

const lines = [];

const goodNewlineMode = true;
// true if manually fixing newlines (probably using stripNewlines.ahk)

let currentLine = [];
pdf.split('\n').forEach((l) => {
    let adjusted = l;

    adjusted = adjusted.replaceAll('‑', '-');
    adjusted = adjusted.replaceAll('’', '\'');
    
    if (l.match(/DC \d{1,2}/)) {
        const dc = l.match(/DC (\d{1,2}) ([^ ]+)/)[1];
        const test = l.match(/DC (\d{1,2}) ([^ ]+)/)[2];
        adjusted = adjusted.replace(/DC (\d{1,2}) ([^ ]+)/, `@Check[type:${test.toLowerCase()}|dc:${dc}]`)
    }


    if (!goodNewlineMode) {
        if (adjusted.startsWith('Creatures: ') || adjusted.startsWith('Treasure: ') || adjusted.startsWith('Hazard: ') || adjusted.startsWith('Side Quest: ')) {
            lines.push(currentLine.join(' '));
            currentLine = [];
        }
    }
    
    currentLine.push(adjusted);
    
    if (goodNewlineMode) {
        if (currentLine.length > 0) {
            lines.push(currentLine.join(' '));
            currentLine = [];
        }
    } else {
        if (l.length < 20) {
            lines.push(currentLine.join(' '));
            currentLine = [];
        }
    }
    
})

if (lines[lines.length -1] !== currentLine.join(' ')) {
    lines.push(currentLine.join(' '));
}

console.log(lines.join('\n'));