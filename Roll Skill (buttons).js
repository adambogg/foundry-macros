const skillTranslation = {
    Acrobatics: 'acr',
    Appraise: 'apr',
    Artistry: 'art',
    Bluff: 'blf',
    Climb: 'clm',
    Craft: 'crf',
    Diplomacy: 'dip',
    'Disable Device': 'dev',
    Disguise: 'dis',
    'Escape Artist': 'esc',
    Fly: 'fly',
    'Handle Animal': 'han',
    Heal: 'hea',
    Intimidate: 'int',
    Linguistics: 'lin',
    Lore: 'lor',
    Perception: 'per',
    Perform: 'prf',
    Profession: 'pro',
    Ride: 'rid',
    'Sense Motive': 'sen',
    'Sleight of Hand': 'slt',
    Spellcraft: 'spl',
    Stealth: 'ste',
    Survival: 'sur',
    Swim: 'swm',
    'Use Magic Device': 'umd',
}

const tokens = canvas.tokens.controlled
let actors = tokens.map((o) => o.actor)
if (!actors.length && c.actorNames.length)
    actors = game.actors.entities.filter((o) => c.actorNames.includes(o.name))
if (!actors.length)
    actors = game.actors.entities.filter(
        (o) => o.isPC && o.hasPerm(game.user, 'OWNER')
    )
actors = actors.filter((o) => o.hasPerm(game.user, 'OWNER'))

const _roll = async function (type) {
    for (let o of actors) {
        await o.rollSkill(type, { event: new MouseEvent({}), skipDialog: true })
    }
}

const buttons = {}
Object.keys(skillTranslation).forEach((skill) => {
    buttons[skill] = {
        label: skill,
        callback: () => {
            _roll(skillTranslation[skill])
        },
    }
})

new Dialog({
    title: 'Roll Knowledge!',
    content: `<p>Choose a knowledge skill</p>`,
    buttons: buttons,
}).render(true)