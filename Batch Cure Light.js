// CONFIGURATION
// Leave the actorNames array empty to guess the players
// Example actorNames: `actorNames: ["Bob", "John"],`
const c = {
  actorNames: [],
};
// END CONFIGURATION

function speak(message) {
  const chatData = {
    user: game.user._id,
    speaker: game.user,
    content: message,
  };
  ChatMessage.create(chatData);
}

function rollCureLight(num, patients) {
  const rollString = `${num}d8 + ${num}`;
  const msg = `Using a total of ${num * patients.length} charges.`;
  speak(msg);

  patients.forEach((patient) => {
    const roll = new Roll(rollString);
    roll.roll();
    roll.toMessage({
      flavor: `Healing ${patient.name}`,
      speaker: {alias: caster.data.name}
    });
  });
}

const tokens = canvas.tokens.controlled;
let actors = tokens.map((o) => o.actor);
if (!actors.length && c.actorNames.length)
  actors = game.actors.entities.filter((o) => c.actorNames.includes(o.name));
if (!actors.length)
  actors = game.actors.entities.filter(
    (o) => o.isPC && o.hasPerm(game.user, "OBSERVER")
  );
// TODO do this (along with speaker: {alias: caster.actor.data.name}) for the other macros, prints character name instead of user's
const caster = canvas.tokens.controlled.map((t) => t.actor).filter((a) => a.hasPerm(game.user, "OWNER"))[0];
actors = actors.filter((o) => o.hasPerm(game.user, "OBSERVER"));

if (!actors.length) {
  ui.notifications.warn("No applicable actor(s) found");
} else {
  const pronoun = actors.length > 1 ? "characters" : "character";
  // TODO map each PC to a toggle button. Make prep function (that calls rollCureLight) compile list of enabled buttons (thus generating list of characters to generate healing for)
  const msg = `Choose a number of times to roll <i>cure light wounds</i> for the following ${pronoun}: <strong>${actors
    .map((o) => o.name)
    .join("</strong>, <strong>")}</strong>`;

  new Dialog({
    title: "Batch Heal",
    content: `<p>${msg}</p>`,
    buttons: {
      one: {
        label: "1",
        callback: () => rollCureLight(1, actors),
      },
      low: {
        label: "5",
        callback: () => rollCureLight(5, actors),
      },
      medium: {
        label: "10",
        callback: () => rollCureLight(10, actors),
      },
      high: {
        label: "15",
        callback: () => rollCureLight(15, actors),
      },
    },
  }).render(true);
}
