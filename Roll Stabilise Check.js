const tokens = canvas.tokens.controlled;

if (tokens.length !== 1) {
  ui.notifications.warn("Select a token.");
} else {
  const actor = tokens[0].actor;
  const conMod = actor.data.data.abilities.con.mod;
  const hp = actor.data.data.attributes.hp.value;

  const penalty = hp < 0 ? Math.abs(hp) : 0;

  const roll = new Roll(`1d20 + ${conMod} - ${penalty}`);
  roll.roll();
  roll.toMessage({
    flavor: `Rolling a stabilise check! DC 10.`,
    speaker: { alias: actor.data.name },
  });
}
