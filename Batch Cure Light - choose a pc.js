const numCharges = 5;

const caster = canvas.tokens.controlled.map((t) => t.actor).filter((a) => a.hasPerm(game.user, "OWNER"))[0];

function rollCureLight(patient) {
  const casterName = caster.data.name;
  const pronoun = casterName === patient.name ? "themselves" : patient.name;
  
  const rollString = `${numCharges}d8 + ${numCharges}`;
  const roll = new Roll(rollString);

  roll.roll();
  roll.toMessage({
    flavor: `Healing ${pronoun} with ${numCharges} charges.`,
    speaker: {alias: casterName}
  });
}

const actors = game.actors.entities.filter(
  (o) => o.isPC && o.hasPerm(game.user, "OBSERVER")
);


if (!actors.length) {
  ui.notifications.warn("No applicable actor(s) found");
} else {
  const msg = `Choose which character to heal with ${numCharges} charges of <i>cure light wounds</i>`;

  const buttons = {};
  actors.forEach((actor) => {
    buttons[actor.name] = {
      label: actor.name.split(" ")[0],
      callback: () => {
        rollCureLight(actor);
      },
    };
  });

  new Dialog({
    title: "Heal Someone",
    content: `<p>${msg}</p>`,
    buttons: buttons,
  }).render(true);
}
