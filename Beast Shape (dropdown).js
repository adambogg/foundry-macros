// If you use Wild Shape, use the Wild Shape macro instead.

// SETUP NEEDED: for each transformation option you want, please enter it as below
// you also need to set imageFolder to the location of where you store your tokens.
// token art is selected by name & as a png, so for example the image for Bat is located at 'assets/tokens/Bestiary/Bat.png'

// you will also need to create a buff on your character sheet called "Wild Shape (Auto)".
// This buff needs no details or changes & changes will be automatically overwritten

// this macro will automatically unequip (and re-equip on Revert) your armour. It will check if the text "Wild Armor"
// is in the armor description or title (and if so, will not unequip it). The macro will do the same for shields (and will
// search them for "Wild Armor" )

// for magical beasts (via beast shape 3 & 4), you will need to add another line to each magical beast's config, as seen on Death Worm
// this line is magical: true (setting it to false or null will make the macro treat it as a normal beast)

const imageFolder = "assets/tokens/Bestiary";

const originalImage = "assets/tokens/PCs/Jenne2.png";
const originalSize = "medium";

const transformationOptions = {
  Bat: {
    size: "diminutive",
    spellSource: "beast shape",
  },
  "Giant Squid": {
    size: "huge",
    spellSource: "beast shape",
  },
  "Death Worm": {
    size: "large",
    spellSource: "beast shape",
    magical: true,
  },
};

const sizes = ["diminutive", "tiny", "small", "medium", "large", "huge"];

const sizeTranslation = {
  fine: "fine",
  diminutive: "dim",
  tiny: "tiny",
  small: "sm",
  medium: "med",
  large: "lg",
  huge: "huge",
  gargantuan: "grg",
  colossal: "col",
};

const createSizeObject = ({ w, h, scale }) => {
  return {
    width: w,
    height: h,
    scale,
  };
};

const mediumOrBigger = (size) => {
  return sizes.indexOf(size) >= sizes.indexOf("medium");
};

const createAbilityChange = (formula, subTarget) => {
  formula = "" + formula;
  return {
    formula,
    priority: 1,
    target: "ability",
    subTarget,
    modifier: "size",
  };
};

const createNACChange = (formula) => {
  formula = "" + formula;
  return {
    formula,
    priority: 1,
    target: "ac",
    subTarget: "nac",
    modifier: "size",
  };
};

const createSpeedChange = (formula, subTarget) => {
  formula = "" + formula;
  return {
    formula,
    priority: 1,
    target: "speed",
    subTarget,
    modifier: "size",
  };
};

const createBuffChanges = (name) => {
  const creature = transformationOptions[name];
  const bulking = mediumOrBigger(creature.size);
  let buffLevel = null;

  const changes = [];

  if (bulking) {
    buffLevel = sizes.indexOf(creature.size) - sizes.indexOf("medium") + 1;
  } else {
    buffLevel = sizes.indexOf("medium") - sizes.indexOf(creature.size);
  }

  let mainBonus, nacBonus, penalty, mainBonusAbility, penaltyAbility;

  if (creature.magical) {
    if (creature.size === "medium") {
      changes.push(createAbilityChange(buffLevel * 4, "str"));
      changes.push(createNACChange(buffLevel * 4));
    } else if (creature.size === "large") {
      changes.push(createAbilityChange(buffLevel * 3, "str"));
      changes.push(createAbilityChange(buffLevel, "con"));
      changes.push(createAbilityChange(-buffLevel, "dex"));
      changes.push(createNACChange(buffLevel * 3));
    } else {
      changes.push(createAbilityChange(buffLevel * 4, "dex"));
      changes.push(createNACChange(buffLevel + 1));

      if (buffLevel > 1) {
        changes.push(createAbilityChange(-buffLevel, "str"));
      }
    }
  } else {
    mainBonus = buffLevel * 2;
    nacBonus = bulking ? buffLevel * 2 : 1;
    penalty = buffLevel > 1 ? -(buffLevel * 2 - 2) : 0;
    mainBonusAbility = bulking ? "str" : "dex";
    penaltyAbility = bulking ? "dex" : "str";

    changes.push(createAbilityChange(mainBonus, mainBonusAbility));
    changes.push(createAbilityChange(penalty, penaltyAbility));
    changes.push(createNACChange(nacBonus));
  }

  return changes;
};

const getWSBuff = (caster) => {
  return caster.items.find(
    (i) =>
      i.type === "buff" &&
      i.name.toLowerCase() === "Wild Shape (Auto)".toLowerCase()
  );
};

const setArmourAndShieldEquipStatus = (caster, status) => {
  caster.items
    .filter(
      (item) =>
        item.data.type === "equipment" &&
        (item.data.data.equipmentType === "armor" ||
          item.data.data.equipmentType === "shield")
    )
    .forEach((item) => {
      // when disabling an item, only disable it if it doesn't have "wild armo[u]r in its body somewhere. Always enable it.
      if (
        !status &&
        JSON.stringify(item)
          .toLowerCase()
          .match(/wild armou{0,1}r/) === null
      ) {
        item.update({ "data.equipped": status });
      } else {
        item.update({ "data.equipped": true });
      }
    });
};

const transform = (caster, token, name) => {
  const creature = transformationOptions[name];
  const changes = createBuffChanges(name);

  const buff = getWSBuff(caster);
  buff.update({ "data.changes": changes });
  buff.update({ "data.active": true });

  token.update({ img: `${imageFolder}/${name}.png` });
  caster.update({ "data.traits.size": sizeTranslation[creature.size] });

  setArmourAndShieldEquipStatus(caster, false);
};

const revertForm = (caster, token) => {
  getWSBuff(caster).update({ "data.active": false });
  token.update({ img: originalImage });
  caster.update({ "data.traits.size": sizeTranslation[originalSize] });
  setArmourAndShieldEquipStatus(caster, true);
};

const tokens = canvas.tokens.controlled;

if (tokens.length !== 1) {
  ui.notifications.warn("Select a token.");
} else {
  const token = tokens[0];
  const actor = token.actor;
  const caster = actor.data;

  const msg = `Choose a creature<br><center><select name="options" id="options">${Object.keys(
    transformationOptions
  )
    .map((name) => `<option value="${name}">${name}</option>`)
    .join("")}</select></center><br>`;

  new Dialog({
    title: "Beast Shape",
    content: msg,
    buttons: {
      go: {
        label: "Transform",
        callback: (html) => {
          const selected = html
            .find("select[id=options]")
            .find(":selected")
            .text();
          transform(actor, token, selected);
        },
      },
      revert: {
        label: "Revert",
        icon: '<i class="fas fa-user"></i>',
        callback: () => revertForm(actor, token),
      },
    },
  }).render(true);
}
