function printDice(formula, rolledDice) {
  const r = new Roll("1d1");
  r.roll();

  const resultDice = r.parts;
  resultDice.rolls = [];
  let total = 0;
  for (let i = 0; i < rolledDice.length; i += 1) {
    const dieResult = rolledDice[i];
    resultDice.rolls.push({ roll: dieResult });
    total += dieResult;
  }

  r.parts = resultDice;
  r._dice = resultDice;
  r._result = String(total);
  r._total = total;
  r._formula = formula;
  r.formula = formula;
  console.log(r);
  r.toMessage();
}

printDice("3d3", [3, 3, 3]);
// ! so much garbage in the json, going to have to replace almost all of it
// ! dogshit approach. use Roll.fromData(data) or roll.toMessage(data, {create: false}), then create ChatMessage from that