// SETUP: set the two variables below to correct values.

const originalImage = "assets/tokens/PCs/Vinukorov.png";
const originalSize = "medium";

const actors = game.actors.entities.filter(
  (o) => o.isPC && o.hasPerm(game.user, "OBSERVER")
);
const tokens = canvas.tokens.controlled;

const getWSBuff = (caster) => {
  return caster.items.find(
    (i) =>
      i.type === "buff" &&
      i.name.toLowerCase() === "Wild Shape (Auto)".toLowerCase()
  );
};

const createSizeObject = ({ w, h, scale }) => {
  return {
    width: w,
    height: h,
    scale,
  };
};

const setArmourAndShieldEquipStatus = (caster, status) => {
  caster.items
    .filter(
      (item) =>
        item.data.type === "equipment" &&
        (item.data.data.equipmentType === "armor" ||
          item.data.data.equipmentType === "shield")
    )
    .forEach((item) => {
      // when disabling an item, only disable it if it doesn't have "wild armo[u]r in its body somewhere. Always enable it.
      if (
        !status &&
        JSON.stringify(item)
          .toLowerCase()
          .match(/wild armou{0,1}r/) === null
      ) {
        item.update({ "data.equipped": status });
      } else {
        item.update({ "data.equipped": true });
      }
    });
};

const sizeTranslation = {
  fine: "fine",
  diminutive: "dim",
  tiny: "tiny",
  small: "sm",
  medium: "med",
  large: "lg",
  huge: "huge",
  gargantuan: "grg",
  colossal: "col",
};

const revertForm = (caster, token) => {
  getWSBuff(caster).update({ "data.active": false });
  token.update({ img: originalImage });
  caster.update({ "data.traits.size": sizeTranslation[originalSize] });
  setArmourAndShieldEquipStatus(caster, true);
};

if (tokens.length !== 1) {
  ui.notifications.warn("Select a token.");
} else {
  const token = tokens[0];
  const actor = token.actor;
  revertForm(actor, token);
}
