const skillTranslation = {
  Acrobatics: "acr",
  Appraise: "apr",
  Artistry: "art",
  Bluff: "blf",
  Climb: "clm",
  Diplomacy: "dip",
  "Disable Device": "dev",
  Disguise: "dis",
  "Escape Artist": "esc",
  Fly: "fly",
  "Handle Animal": "han",
  Heal: "hea",
  Intimidate: "int",
  Linguistics: "lin",
  Lore: "lor",
  Perception: "per",
  Ride: "rid",
  "Sense Motive": "sen",
  "Sleight of Hand": "slt",
  Spellcraft: "spl",
  Stealth: "ste",
  Survival: "sur",
  Swim: "swm",
  "Use Magic Device": "umd",
};

const tokens = canvas.tokens.controlled;
let actors = tokens.map((o) => o.actor);
if (!actors.length && c.actorNames.length)
  actors = game.actors.entities.filter((o) => c.actorNames.includes(o.name));
if (!actors.length)
  actors = game.actors.entities.filter(
    (o) => o.isPC && o.hasPerm(game.user, "OWNER")
  );
actors = actors.filter((o) => o.hasPerm(game.user, "OWNER"));

const _roll = async function (type) {
  for (let o of actors) {
    await o.rollSkill(type, { event: new MouseEvent({}), skipDialog: true });
  }
};

const msg = `Select which skill to roll:<br><br><center><select name="skills" id="skills">${Object.keys(
  skillTranslation
)
  .map((name) => `<option value="${name}">${name}</option>`)
  .join("")}</select></center><br>`;

new Dialog({
  title: "Roll a skill!",
  content: msg,
  buttons: {
    go: {
      label: "Roll",
      callback: (html) => {
        const selected = html.find("select[id=skills]").find(":selected").text();
        console.log(selected);
        _roll(skillTranslation[selected]);
      },
    },
  },
}).render(true);
