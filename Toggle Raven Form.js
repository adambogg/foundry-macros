const becomeRavenFormMessage = "Going large!";
const becomeNimlyFormMessage = "Here I go stabbing again";
const ravenSize = "large";

const sizeTranslation = {
  fine: "fine",
  diminutive: "dim",
  tiny: "tiny",
  small: "sm",
  medium: "med",
  large: "lg",
  huge: "huge",
  gargantuan: "grg",
  colossal: "col",
};

const createSizeObject = ({ w, h, scale }) => {
  return {
    width: w,
    height: h,
    scale,
  };
};

const getRavenFormBuff = (actor) => {
  return actor.items.find(
    (i) => i.type === "buff" && i.name === "Raven Form (Auto)"
  );
};

const updateRavenFormBuff = (actor, active) => {
  getRavenFormBuff(actor).update({ "data.active": active });
};

const chat = (actor, message) => {
  ChatMessage.create({
    user: game.user._id,
    speaker: { alias: actor.data.name },
    content: message,
  });
};

const toggleRavenForm = (token) => {
  const actor = token.actor;
  const becomingRaven = !getRavenFormBuff(actor).data.data.active;

  const changeMessage = becomingRaven
    ? becomeRavenFormMessage
    : becomeNimlyFormMessage;
  chat(actor, changeMessage);

  updateRavenFormBuff(actor, becomingRaven);

  if (becomingRaven) {
    token.update({ img: "worlds/golarion/tokens/PCs/NimlyRaven.png" });
    token.update(
      createSizeObject(CONFIG.PF1.tokenSizes[sizeTranslation[ravenSize]])
    );
    actor.data.data.traits.size = sizeTranslation[ravenSize];
  } else {
    token.update({ img: actor.data.flags.originalData.image });
    token.update(
      createSizeObject(
        CONFIG.PF1.tokenSizes[actor.data.flags.originalData.size]
      )
    );
    actor.data.data.traits.size = actor.data.flags.originalData.size;
  }
};

const tokens = canvas.tokens.controlled;

if (tokens.length !== 1) {
  ui.notifications.warn("Select a token.");
} else {
  const token = tokens[0];
  const actor = token.actor;
  const actorData = actor.data;

  if (!Object.keys(actorData.flags).includes("originalData")) {
    const originalData = {
      image: actorData.token.img,
      size: actorData.data.traits.size,
    };
    actorData.flags.originalData = originalData;
  }

  toggleRavenForm(token);
}
