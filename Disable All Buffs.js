const tokens = canvas.tokens.controlled;

if (tokens.length !== 1) {
  ui.notifications.warn("Select a token.");
} else {
  const token = tokens[0];
  const actor = token.actor;

  actor.items
    .filter((i) => i.type === "buff")
    .forEach((i) => i.update({ "data.active": false }));
}
