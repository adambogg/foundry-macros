(async () => {
    const names = [
        "Damp Entrance",
        "Lair",
    ]

    const journal = await JournalEntry.create({ name: 'Gauntlight Ruins' });
    const pages = names.map(((name, i) => ({ name: `A${i+1} ${name}`, type: 'text' })));
    await journal.createEmbeddedDocuments('JournalEntryPage', pages);
})()