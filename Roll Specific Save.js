// CONFIGURATION
// set which save to roll - fortitude -> fort, reflex -> ref, will -> will.
const save = "fort";
// END CONFIGURATION

const tokens = canvas.tokens.controlled;

if (tokens.length !== 1) {
  ui.notifications.warn("No applicable actor(s) found");
} else {
  tokens[0].actor.rollSavingThrow(save, { event: new MouseEvent({}) });
}
