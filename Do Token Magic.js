const doTokenMagic = (nbImage) => {
  TokenMagic.addUpdateFiltersOnSelected([
    {
      filterType: "images",
      filterId: "mySpectralImages",
      time: 0,
      blend: 2,
      nbImage,
      padding: 150,
      alphaImg: 0.5,
      alphaChr: 0.0,
      ampX: 0.4,
      ampY: 0.4,
      animated: {
        time: {
          speed: 0.005,
          animType: "move",
        },
        ampX: {
          val1: 0,
          val2: 0.22,
          animType: "syncCosOscillation",
          loopDuration: 2500,
        },
        ampY: {
          val1: 0,
          val2: 0.24,
          animType: "syncCosOscillation",
          loopDuration: 2500,
          pauseBetweenDuration: 2500,
        },
        alphaChr: {
          val1: 1,
          val2: 0,
          animType: "syncCosOscillation",
          loopDuration: 2500,
        },
        alphaImg: {
          val1: 0.2,
          val2: 0.8,
          animType: "syncSinOscillation",
          loopDuration: 2500,
        },
      },
    },
  ]);
};

const buttons = {};
const options = [2, 3, 4, 5, 6, 7, 8, 9];

options.forEach((option) => {
  buttons[option] = {
    label: "" + option,
    callback: () => {
      doTokenMagic(option);
    },
  };
});

new Dialog({
  title: "Add effect!",
  content: `<p>Choose an nbImage value</p>`,
  buttons: buttons,
}).render(true);
